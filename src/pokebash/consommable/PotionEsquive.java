package pokebash.consommable;

import java.lang.Math;
import java.util.Objects;

import pokebash.personnage.Personnage;

public class PotionEsquive extends Potion {

    // Attributs
    private int esquiveAugmentee;

    // Constructeur
    public PotionEsquive() {
      this.setNom("Potion d'esquive");
      // esquive augmentée aléatoire entre 5 et 15 inclus
      this.esquiveAugmentee = (int) (Math.random() * 11) + 5;
    }

    // Accesseurs
    public int getEsquiveAugmentee() {
        return esquiveAugmentee;
    }

    public void setEsquiveAugmentee(int esquiveAugmentee) {
        this.esquiveAugmentee = esquiveAugmentee;
    }

    // Méthodes
    public void utiliser(Personnage cible) {
        cible.setEsquive(Math.min(100, cible.getEsquive() + this.esquiveAugmentee));
    }

    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof PotionEsquive)) {
            return false;
        }
        PotionEsquive potionEsquive = (PotionEsquive) o;
        return this.getEsquiveAugmentee() == potionEsquive.getEsquiveAugmentee();
    }

    public int hashCode() {
        return Objects.hash(this.getNom(), this.getEsquiveAugmentee());
    }
}
