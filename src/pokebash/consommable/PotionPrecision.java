package pokebash.consommable;

import java.lang.Math;
import java.util.Objects;

import pokebash.personnage.Personnage;

public class PotionPrecision extends Potion {

    // Attributs
    private int precisionAugmentee;

    // Constructeur
    public PotionPrecision() {
      this.setNom("Potion de précision");
      // précision augmentée aléatoire entre 5 et 15 inclus
      this.precisionAugmentee = (int) (Math.random() * 11) + 5;
    }

    // Accesseurs
    public int getPrecisionAugmentee() {
        return precisionAugmentee;
    }

    public void setPrecisionAugmentee(int precisionAugmentee) {
        this.precisionAugmentee = precisionAugmentee;
    }

    // Méthodes
    public void utiliser(Personnage cible) {
      cible.setPrecision(Math.min(100, cible.getPrecision() + this.precisionAugmentee));
    }

    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof PotionPrecision)) {
            return false;
        }
        PotionPrecision potionPrecision = (PotionPrecision) o;
        return this.getPrecisionAugmentee() == potionPrecision.getPrecisionAugmentee();
    }

    public int hashCode() {
        return Objects.hash(this.getNom(), this.getPrecisionAugmentee());
    }
}
