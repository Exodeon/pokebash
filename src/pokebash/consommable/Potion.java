package pokebash.consommable;

import pokebash.personnage.Personnage;

public abstract class Potion extends Consommable {

    public abstract void utiliser(Personnage cible);
}
