package pokebash.consommable;

import java.lang.Math;
import java.util.Objects;

import pokebash.personnage.Personnage;

public class PotionSoin extends Potion {

    // Attributs
    private int pointsDeVieSoignes;

    // Constructeur
    public PotionSoin() {
      this.setNom("Potion de soin");
      // esquve augmentée aléatoire entre 5 et 15 inclus
      this.pointsDeVieSoignes = (int) (Math.random() * 11) + 15;
    }

    // Accesseurs
    public int getPointsDeVieSoignes() {
        return pointsDeVieSoignes;
    }

    public void setPointsDeVieSoignes(int pointsDeVieSoignes) {
        this.pointsDeVieSoignes = pointsDeVieSoignes;
    }

    // Méthodes
    public void utiliser(Personnage cible) {
      cible.setPointsDeVie(Math.min(cible.getPointsDeVieMax(), cible.getPointsDeVie() + this.pointsDeVieSoignes));
    }

    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof PotionSoin)) {
            return false;
        }
        PotionSoin potionSoin = (PotionSoin) o;
        return this.getPointsDeVieSoignes() == potionSoin.getPointsDeVieSoignes();
    }

    public int hashCode() {
        return Objects.hash(this.getNom(), this.getPointsDeVieSoignes());
    }
}
