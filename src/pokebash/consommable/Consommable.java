package pokebash.consommable;

import java.util.Objects;

public abstract class Consommable {

    // Attributs
    private String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof Consommable)) {
            return false;
        }
        Consommable consommable = (Consommable) o;
        return this.getNom() == consommable.getNom();
    }

    public int hashCode() {
        return Objects.hash(this.getNom());
    }
}
