package pokebash;

import pokebash.personnage.Personnage;
import pokebash.jeu.JeuTourParTourDeuxJoueurs;
import pokebash.jeu.JoueurPokebash;
import pokebash.jeu.Joueur;
import pokebash.ihm.Fenetre;

import java.util.ArrayList;

public class Pokebash extends JeuTourParTourDeuxJoueurs {

    private Fenetre fenetre;

    public Pokebash(){
        this.setJoueurs(new ArrayList<Joueur>());
        this.getJoueurs().add(new JoueurPokebash());
        this.getJoueurs().add(new JoueurPokebash());
        this.setJoueurCourant(this.getJoueurs().get(0));
        this.fenetre = new Fenetre(this);
    }

    public void jouerTour() {
        ((JoueurPokebash) this.getJoueurCourant()).initPersosAJouer();
        this.fenetre.majFenetreNouveauPerso(this);
        while (!(((JoueurPokebash) this.getJoueurCourant()).getPersosAJouer().isEmpty())){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Fenetre getFenetre(){
        return this.fenetre;
    }

    public boolean estFini() {
        return ((JoueurPokebash) this.getJoueurCourant()).aPerdu();
    }



    public static void main(String[] args){
        Pokebash pokebash = new Pokebash();
        pokebash.jouer();
        pokebash.getFenetre().ecranVictoire();
    }

}
