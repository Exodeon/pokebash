package pokebash.personnage;

import java.util.ArrayList;

import pokebash.personnage.action.attaque.AttaqueVoleur;
import pokebash.personnage.action.soin.AucunSoin;
import pokebash.personnage.action.fouille.FouilleVoleur;
import pokebash.personnage.action.defense.Defense;
import pokebash.consommable.Consommable;

public class Voleur extends Personnage {

    private final static int PV_MAX = 40;
    private final static int PRECISION = 90;
    private final static int ESQUIVE = 30;
    // Constructeur
    public Voleur(String nom) {
        super(nom, PV_MAX, PRECISION, ESQUIVE, new ArrayList<Consommable>(), new AttaqueVoleur(), new AucunSoin(), new FouilleVoleur(), new Defense());
        // on verra plus tard si on ajoute des objets dès le début
        this.getDefense().setPersonnageCourant(this);
    }

    public String toString(){
        return "Voleur" + super.toString();
    }

    public Personnage clone(){
        Personnage clone = new Voleur(this.getNom());
        clone.setPointsDeVie(this.getPointsDeVie());
        clone.setEsquive(this.getEsquive());
        clone.setPrecision(this.getPrecision());
        clone.setInventaire(this.getInventaire());
        return clone;
    }
}
