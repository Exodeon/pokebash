package pokebash.personnage;

import java.util.ArrayList;

import pokebash.personnage.action.attaque.AttaqueMage;
import pokebash.personnage.action.soin.AucunSoin;
import pokebash.personnage.action.fouille.FouilleBasique;
import pokebash.personnage.action.defense.Defense;
import pokebash.consommable.Consommable;

public class Mage extends Personnage {

    private final static int PV_MAX = 30;
    private final static int PRECISION = 80;
    private final static int ESQUIVE = 20;
    // Constructeur
    public Mage(String nom) {
        super(nom, PV_MAX, PRECISION, ESQUIVE, new ArrayList<Consommable>(), new AttaqueMage(), new AucunSoin(), new FouilleBasique(), new Defense());
        // on verra plus tard si on ajoute des objets dès le début
        this.getDefense().setPersonnageCourant(this);
    }

    public String toString(){
        return "Mage" + super.toString();
    }

    public Personnage clone(){
        Personnage clone = new Mage(this.getNom());
        clone.setPointsDeVie(this.getPointsDeVie());
        clone.setEsquive(this.getEsquive());
        clone.setPrecision(this.getPrecision());
        clone.setInventaire(this.getInventaire());
        return clone;
    }
}
