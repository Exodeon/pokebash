package pokebash.personnage;

import java.lang.Math;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import pokebash.Pokebash;
import pokebash.consommable.Consommable;
import pokebash.consommable.Potion;
import pokebash.consommable.Arme;
import pokebash.consommable.Bouclier;
import pokebash.personnage.action.attaque.Attaque;
import pokebash.personnage.action.defense.Defense;
import pokebash.personnage.action.soin.Soin;
import pokebash.personnage.action.soin.AucunSoin;
import pokebash.personnage.action.fouille.Fouille;
import pokebash.personnage.action.ChoixAction;

public abstract class Personnage {

    // Attributs
    private int pointsDeVie;
    private int pointsDeVieMax;
    private ArrayList<Consommable> inventaire;
    private Attaque attaque;
    private Soin soin;
    private Fouille fouille;
    private Defense defense;
    private int precision;
    private int esquive;
    private String nom;

    // Constructeur

    public Personnage(String nom, int pointsDeVieMax, int precision, int esquive, ArrayList<Consommable> inventaire, Attaque attaque, Soin soin, Fouille fouille, Defense defense){
        this.nom = nom;
        this.pointsDeVieMax = pointsDeVieMax;
        this.pointsDeVie = this.pointsDeVieMax;
        this.precision = precision;
        this.esquive = esquive;
        this.inventaire = inventaire;
        this.attaque = attaque;
        this.soin = soin;
        this.fouille = fouille;
        this.defense = defense;
    }


    // Accesseurs
    public int getPointsDeVie() {
        return pointsDeVie;
    }

    public void setPointsDeVie(int pointsDeVie) {
        this.pointsDeVie = pointsDeVie;
    }

    public int getPointsDeVieMax() {
        return pointsDeVieMax;
    }

    public void setPointsDeVieMax(int pointsDeVieMax) {
        this.pointsDeVieMax = pointsDeVieMax;
    }

    public ArrayList<Consommable> getInventaire() {
        return inventaire;
    }

    public void setInventaire(ArrayList<Consommable> inventaire) {
        this.inventaire = inventaire;
    }

    public Attaque getAttaque() {
        return attaque;
    };

    public void setAttaque(Attaque attaque) {
        this.attaque = attaque;
    }

    public Soin getSoin() {
        return soin;
    }

    public void setSoin(Soin soin) {
        this.soin = soin;
    }

    public Fouille getFouille() {
        return fouille;
    }

    public void setFouille(Fouille fouille) {
        this.fouille = fouille;
    }

    public Defense getDefense() {
        return defense;
    }

    public void setDefense(Defense defense) {
        this.defense = defense;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    public int getEsquive() {
        return esquive;
    }

    public void setEsquive(int esquive) {
        this.esquive = esquive;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public abstract Personnage clone();

    // Méthodes
    public void attaquer(Personnage cible) {
        if (!cible.getInventaire().contains(new Bouclier())) {
            float pourcentageATester = ((float)this.getPrecision() / 100) * ((100 - (float)cible.getEsquive()) / 100);
            if (Math.random() <= pourcentageATester) {
                int degatsAttaque = this.getAttaque().attaquer();
                if (this.getInventaire().contains(new Arme())) {
                    degatsAttaque += Arme.DEGATS;
                    this.getInventaire().remove(new Arme());
                }
                int vieRestante = Math.max(0, cible.getPointsDeVie() - degatsAttaque);
                cible.setPointsDeVie(vieRestante);
            }
        } else {
            cible.getInventaire().remove(new Bouclier());
        }
    }

    public void soigner(Personnage cible) {
        int vieAtteinte = Math.min(cible.getPointsDeVieMax(), cible.getPointsDeVie() + this.getSoin().soigner());
        cible.setPointsDeVie(vieAtteinte);
    }

    public void defendre(Personnage cible) {
        this.getDefense().defendre(cible);
    }

    public void seReplier() {
        this.getDefense().seReplier();
    }

    public void fouiller() {
        Consommable objetTrouve = this.getFouille().fouiller();
        if (objetTrouve != null) {
            this.getInventaire().add(objetTrouve);
        }
    }

    public void utiliserObjet(Potion potion) {
        // TODO exception si l'objet n'est pas dans l'inventaire
        if (this.getInventaire().contains(potion)) {
            potion.utiliser(this);
            this.getInventaire().remove(potion);
        }
    }

    public void donnerObjet(Consommable objet, Personnage destinaire) {
      // TODO exception si l'objet n'est pas dans l'inventaire
        if (this.getInventaire().contains(objet)) {
            this.getInventaire().remove(objet);
            destinaire.getInventaire().add(objet);
        }
    }

    public String toString(){
        return " : \nNom : " + this.getNom() + "\nPV  : " + this.getPointsDeVie() + "/" + this.getPointsDeVieMax() + "\nPrécision : "
        + this.getPrecision() + "\nEsquive : " + this.getEsquive() + "\nAttaque : " + this.getAttaque() + "\nSoin : " + this.getSoin()
        + "\nFouille : " + this.getFouille() + "\nDefense : " + this.getDefense();
    }

    public String resumePersonnage() {
        return this.getNom() + "\nPV  : " + this.getPointsDeVie() + "/" + this.getPointsDeVieMax() + "\nPrécision : "
        + this.getPrecision() + "\nEsquive : " + this.getEsquive() + "\nDégâts : " + this.getAttaque().attaquer() + "\n"
        + this.getFouille() + "\n" + this.getSoin();
    }

    public boolean equals(Personnage autrePersonnage){
        if (autrePersonnage == null)
            return false;
        else
            return (this.getNom() == autrePersonnage.getNom());
    }

}
