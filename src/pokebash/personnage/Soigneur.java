package pokebash.personnage;

import java.util.ArrayList;

import pokebash.personnage.action.attaque.AttaqueSoigneur;
import pokebash.personnage.action.soin.SoinSoigneur;
import pokebash.personnage.action.fouille.FouilleBasique;
import pokebash.personnage.action.defense.Defense;
import pokebash.consommable.Consommable;

public class Soigneur extends Personnage {

    private final static int PV_MAX = 30;
    private final static int PRECISION = 100;
    private final static int ESQUIVE = 15;
    // Constructeur
    public Soigneur(String nom) {
        super(nom, PV_MAX, PRECISION, ESQUIVE, new ArrayList<Consommable>(), new AttaqueSoigneur(), new SoinSoigneur(), new FouilleBasique(), new Defense());
        // on verra plus tard si on ajoute des objets dès le début
        this.getDefense().setPersonnageCourant(this);
    }

    public String toString(){
        return "Soigneur" + super.toString();
    }

    public Personnage clone(){
        Personnage clone = new Soigneur(this.getNom());
        clone.setPointsDeVie(this.getPointsDeVie());
        clone.setEsquive(this.getEsquive());
        clone.setPrecision(this.getPrecision());
        clone.setInventaire(this.getInventaire());
        return clone;
    }
}
