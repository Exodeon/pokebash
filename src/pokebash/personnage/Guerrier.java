package pokebash.personnage;

import java.util.ArrayList;

import pokebash.personnage.action.attaque.AttaqueGuerrier;
import pokebash.personnage.action.soin.AucunSoin;
import pokebash.personnage.action.fouille.FouilleBasique;
import pokebash.personnage.action.defense.Defense;
import pokebash.consommable.Consommable;

public class Guerrier extends Personnage {

    private final static int PV_MAX = 70;
    private final static int PRECISION = 95;
    private final static int ESQUIVE = 10;

    // Constructeur
    public Guerrier(String nom) {

        super(nom, PV_MAX, PRECISION, ESQUIVE, new ArrayList<Consommable>(), new AttaqueGuerrier(), new AucunSoin(), new FouilleBasique(), new Defense());
        // on verra plus tard si on ajoute des objets dès le début
        this.getDefense().setPersonnageCourant(this);
    }

    public String toString(){
        return "Guerrier" + super.toString();
    }

    public Personnage clone(){
        Personnage clone = new Guerrier(this.getNom());
        clone.setPointsDeVie(this.getPointsDeVie());
        clone.setEsquive(this.getEsquive());
        clone.setPrecision(this.getPrecision());
        clone.setInventaire(this.getInventaire());
        return clone;
    }
}
