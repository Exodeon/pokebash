package pokebash.personnage.action.soin;

public class AucunSoin implements Soin {

    private final int PV_SOIGNE = 0;

    public int soigner(){
        return PV_SOIGNE;
    }

    public String toString(){
        return "Aucun soin";
    }
}
