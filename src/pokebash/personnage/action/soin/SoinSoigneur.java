package pokebash.personnage.action.soin;

public class SoinSoigneur implements Soin {

    private final int PV_SOIGNE = 20;

    public int soigner(){
        return PV_SOIGNE;
    }

    public String toString(){
        return PV_SOIGNE + " PV soignés";
    }
}
