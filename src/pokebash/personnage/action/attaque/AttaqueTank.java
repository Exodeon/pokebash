package pokebash.personnage.action.attaque;

public class AttaqueTank implements Attaque {

    private final int DEGATS = 7;

    public AttaqueTank(){
    }

    public int attaquer(){
        return this.DEGATS;
    }

    public String toString(){
        return DEGATS + " dégâts";
    }
}
