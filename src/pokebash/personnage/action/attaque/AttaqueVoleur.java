package pokebash.personnage.action.attaque;

public class AttaqueVoleur implements Attaque {

    private final int DEGATS = 15;

    public AttaqueVoleur(){
    }

    public int attaquer(){
        return this.DEGATS;
    }

    public String toString(){
        return DEGATS + " dégâts";
    }
}
