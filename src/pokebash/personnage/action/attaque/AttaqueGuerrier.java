package pokebash.personnage.action.attaque;

public class AttaqueGuerrier implements Attaque {

    private final int DEGATS = 20;

    public AttaqueGuerrier(){
    }

    public int attaquer(){
        return this.DEGATS;
    }

    public String toString(){
        return DEGATS + " dégâts";
    }
}
