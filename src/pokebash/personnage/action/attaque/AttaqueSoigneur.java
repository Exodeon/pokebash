package pokebash.personnage.action.attaque;

public class AttaqueSoigneur implements Attaque {

    private final int DEGATS = 5;

    public AttaqueSoigneur(){
    }

    public int attaquer(){
        return this.DEGATS;
    }

    public String toString(){
        return DEGATS + " dégâts";
    }
}
