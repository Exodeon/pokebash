package pokebash.personnage.action.attaque;

public class AttaqueMage implements Attaque {

    private final int DEGATS = 35;

    public AttaqueMage(){
    }

    public int attaquer(){
        return this.DEGATS;
    }

    public String toString(){
        return DEGATS + " dégâts";
    }
}
