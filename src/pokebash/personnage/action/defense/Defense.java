package pokebash.personnage.action.defense;

import pokebash.personnage.Personnage;

public class Defense {

    // Attributs
    private Personnage personnageCourant;
    private Personnage defenseur;
    private Personnage defendu;

    // Constructeur

    public Defense(){
        this.personnageCourant = null;
        this.defenseur = null;
        this.defendu = null;
    }

    public Personnage getPersonnageCourant() {
        return this.personnageCourant;
    }

    public void setPersonnageCourant(Personnage personnageCourant) {
        this.personnageCourant = personnageCourant;
    }

    public Personnage getDefenseur() {
        return this.defenseur;
    }

    public void setDefenseur(Personnage defenseur) {
        this.defenseur = defenseur;
    }

    public Personnage getDefendu() {
        return this.defendu;
    }

    public void setDefendu(Personnage defendu) {
        this.defendu = defendu;
    }

    // Méthodes
    public void defendre(Personnage cible) {
        if ((this.getDefenseur() != null) && (this.getDefenseur().equals(cible)))
            this.permuterDefense();
        else if ((cible.getDefense().getDefendu() == null) && (cible.getDefense().getDefenseur() == null) && (this.getDefenseur() == null) && (this.getDefendu() == null)){ // mettre les conditions pour limiter les dégâts
            this.setDefendu(cible);
            cible.getDefense().setDefenseur(this.personnageCourant);
        }
    }

    private void permuterDefense(){
        if (this.getDefendu() != null) {
            this.setDefenseur(this.getDefendu());
            this.setDefendu(null);
            this.getDefenseur().getDefense().setDefendu(this.personnageCourant);
            this.getDefenseur().getDefense().setDefenseur(null);
        }
        else if (this.getDefenseur() != null){
            this.setDefendu(this.getDefenseur());
            this.getDefendu().getDefense().setDefenseur(this.personnageCourant);
            this.getDefenseur().getDefense().setDefendu(null);
            this.setDefenseur(null);
        }

        // rajouter l'exception quand on peut pas permuter
    }

    public void seReplier() {
        if (this.getDefendu() != null) {
            this.getDefendu().getDefense().setDefenseur(null);
            this.setDefendu(null);
        }

        // rajouter l'exception quand on est seul ou defendu
    }

    public String toString(){
        return "(Courant : " + this.getPersonnageCourant().getNom()
            + ", Défendu : " + ((this.getDefendu() != null) ? this.getDefendu().getNom() : "Aucun")
            + ", Defenseur : " + ((this.getDefenseur() != null) ? this.getDefenseur().getNom() : "Aucun") + ")";
    }
}
