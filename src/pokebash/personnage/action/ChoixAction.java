package pokebash.personnage.action;

public enum ChoixAction {
    INVENTAIRE("Inventaire"),
    DONNER("Donner un objet"),
    ATTAQUER("Attaquer"),
    DEFENDRE("Défendre"),
    SE_REPLIER("Se replier"),
    SOIGNER("Soigner"),
    FOUILLER("Fouiller");

    private String nom = "";

    ChoixAction(String nom) {
        this.nom = nom;
    }

    public String toString() {
        return this.nom;
    }
}
