package pokebash.personnage.action.fouille;

import java.lang.Math;
import pokebash.consommable.Consommable;
import pokebash.consommable.Arme;
import pokebash.consommable.Bouclier;
import pokebash.consommable.PotionEsquive;
import pokebash.consommable.PotionPrecision;
import pokebash.consommable.PotionSoin;

public class FouilleVoleur implements Fouille {
    public Consommable fouiller(){
        /*5 rien 25 potionsoin 10 arme 10 bouclier 25 potionEsquive 25 potionPrécision*/
        double unif = Math.random();
        if (unif < 0.05)
            return null;
        else if (unif < 0.3)
            return new PotionSoin();
        else if (unif < 0.4)
            return new Arme();
        else if (unif < 0.5)
            return new Bouclier();
        else if (unif < 0.75)
            return new PotionEsquive();
        else
            return new PotionPrecision();
    }

    public String toString(){
        return "Fouille voleur";
    }
}
