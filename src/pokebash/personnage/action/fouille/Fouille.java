package pokebash.personnage.action.fouille;

import pokebash.consommable.Consommable;

public interface Fouille {
    public Consommable fouiller();
}
