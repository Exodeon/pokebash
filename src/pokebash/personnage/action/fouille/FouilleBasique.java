package pokebash.personnage.action.fouille;

import java.lang.Math;
import pokebash.consommable.Consommable;
import pokebash.consommable.Arme;
import pokebash.consommable.Bouclier;
import pokebash.consommable.PotionEsquive;
import pokebash.consommable.PotionPrecision;
import pokebash.consommable.PotionSoin;

public class FouilleBasique implements Fouille {

    public Consommable fouiller(){
        /*30 rien 20 potionsoin 5 arme 5 bouclier 20 potionEsquive 20 potionPrécision*/
        double unif = Math.random();
        if (unif < 0.3)
            return null;
        else if (unif < 0.5)
            return new PotionSoin();
        else if (unif < 0.55)
            return new Arme();
        else if (unif < 0.6)
            return new Bouclier();
        else if (unif < 0.8)
            return new PotionEsquive();
        else
            return new PotionPrecision();
    }

    public String toString(){
        return "Fouille basique";
    }
}
