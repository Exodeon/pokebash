package pokebash.personnage;

import java.util.ArrayList;

import pokebash.personnage.action.attaque.AttaqueTank;
import pokebash.personnage.action.soin.AucunSoin;
import pokebash.personnage.action.fouille.FouilleBasique;
import pokebash.personnage.action.defense.Defense;
import pokebash.consommable.Consommable;

public class Tank extends Personnage {

    private final static int PV_MAX = 100;
    private final static int PRECISION = 80;
    private final static int ESQUIVE = 5;
    // Constructeur
    public Tank(String nom) {
        super(nom, PV_MAX, PRECISION, ESQUIVE, new ArrayList<Consommable>(), new AttaqueTank(), new AucunSoin(), new FouilleBasique(), new Defense());
        // on verra plus tard si on ajoute des objets dès le début
        this.getDefense().setPersonnageCourant(this);
    }

    public String toString(){
        return "Tank" + super.toString();
    }

    public Personnage clone(){
        Personnage clone = new Tank(this.getNom());
        clone.setPointsDeVie(this.getPointsDeVie());
        clone.setEsquive(this.getEsquive());
        clone.setPrecision(this.getPrecision());
        clone.setInventaire(this.getInventaire());
        return clone;
    }
}
