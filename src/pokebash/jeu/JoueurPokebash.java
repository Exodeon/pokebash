package pokebash.jeu;

import java.util.ArrayList;

import pokebash.personnage.Personnage;
import pokebash.personnage.Guerrier;
import pokebash.personnage.Soigneur;
import pokebash.personnage.Voleur;
import pokebash.personnage.Tank;
import pokebash.personnage.Mage;

import java.util.Scanner;

public class JoueurPokebash extends Joueur {

    // Attributs
    private ArrayList<Personnage> equipe;
    private ArrayList<String> persosAJouer;

    public JoueurPokebash(){
        super();
        equipe = new ArrayList<Personnage>();
        for (int i = 1 ; i <= 3 ; i++){
            System.out.println("\nPersonnage " + i);
            creerPersonnage();
        }
    }

    public ArrayList<Personnage> getEquipe() {
        return equipe;
    }

    public void setEquipe(ArrayList<Personnage> equipe) {
        this.equipe = equipe;
    }

    public ArrayList<String> getPersosAJouer() {
        return persosAJouer;
    }

    public void setPersosAJouer(ArrayList<String> persosAJouer) {
        this.persosAJouer = persosAJouer;
    }

    public void creerPersonnage(){
        Scanner sc = new Scanner(System.in);
        System.out.println("\nNom du personnage : ");
        String nom = sc.nextLine();

        System.out.println("Classes possibles : ");
        System.out.println("1) Guerrier");
        System.out.println("2) Tank");
        System.out.println("3) Soigneur");
        System.out.println("4) Mage");
        System.out.println("5) Voleur");
        System.out.println("Classe de votre personnage (1 à 5) : ");
        int numeroClasse = sc.nextInt();
        sc.nextLine();
        switch (numeroClasse){
            case 1 :
                equipe.add(new Guerrier(nom));
                break;
            case 2 :
                equipe.add(new Tank(nom));
                break;
            case 3 :
                equipe.add(new Soigneur(nom));
                break;
            case 4 :
                equipe.add(new Mage(nom));
                break;
            case 5 :
                equipe.add(new Voleur(nom));
                break;
        }
    }

    public void initPersosAJouer(){
         this.persosAJouer = new ArrayList<String>();
         for (Personnage perso : this.equipe)
            if (perso.getPointsDeVie() != 0)
                this.persosAJouer.add(perso.getNom());
    }

    public boolean aPerdu(){
        boolean perdu = true;
        for (Personnage perso : this.equipe)
           if (perso.getPointsDeVie() != 0)
               perdu = false;

        return perdu;
    }
}
