package pokebash.jeu;

import java.util.Scanner;

public abstract class Joueur{

    private String nom;

    public Joueur(){
        Scanner sc = new Scanner(System.in);
        System.out.println("\nNom du joueur : ");
        this.nom = sc.nextLine();
    }

    public String getNom(){
        return nom;
    }

    public void setNom(String nom){
        this.nom = nom;
    }
}
