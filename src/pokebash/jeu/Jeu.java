package pokebash.jeu;

import java.util.ArrayList;

public abstract class Jeu {
    /*Attribut*/
    private ArrayList<Joueur> joueurs;

    public ArrayList<Joueur> getJoueurs(){
        return this.joueurs;
    }

    public void setJoueurs(ArrayList<Joueur> joueurs){
        this.joueurs = joueurs;
    }

    public abstract void jouer();

    public abstract boolean estFini();

}
