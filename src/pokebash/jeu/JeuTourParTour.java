package pokebash.jeu;

public abstract class JeuTourParTour extends Jeu {

    // AttributS
    private Joueur joueurCourant;
    private int indiceJoueurCourant;


    // Accesseurs
    public Joueur getJoueurCourant(){
        return this.joueurCourant;
    }

    public int getIndiceJoueurCourant(){
        return this.indiceJoueurCourant;
    }

    public void setJoueurCourant(Joueur joueurCourant){
        this.joueurCourant = joueurCourant;
    }

    //Méthodes

    public Joueur joueurSuivant(){
        return this.getJoueurs().get((this.getIndiceJoueurCourant() + 1 < this.getJoueurs().size()) ? this.getIndiceJoueurCourant() + 1 : 0);
    }

    public void changerJoueur(){
        this.joueurCourant = this.joueurSuivant();
        this.indiceJoueurCourant = (this.getIndiceJoueurCourant() + 1 < this.getJoueurs().size()) ? this.getIndiceJoueurCourant() + 1 : 0;
    }

    public void jouer(){
        while (!estFini()){
            jouerTour();
            changerJoueur();
        }
    }

    public abstract void jouerTour();
}
