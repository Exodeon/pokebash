package pokebash;

import java.util.ArrayList;

import pokebash.personnage.Personnage;
import pokebash.personnage.Tank;
import pokebash.personnage.Soigneur;
import pokebash.personnage.Guerrier;
import pokebash.personnage.Mage;
import pokebash.personnage.Voleur;
import pokebash.consommable.PotionEsquive;
import pokebash.consommable.PotionPrecision;
import pokebash.consommable.PotionSoin;
import pokebash.consommable.Bouclier;
import pokebash.consommable.Arme;

public class TestsPerso {

    public static void main(String[] args){
        ArrayList<Personnage> equipe = new ArrayList<Personnage>();
        equipe.add(new Tank("Gimli"));
        equipe.add(new Guerrier("Aragorn"));
        equipe.add(new Soigneur("Galadriel"));
        equipe.add(new Voleur("Bilbon"));
        equipe.add(new Mage("Gandalf"));

        for (Personnage perso : equipe) {
            System.out.println(perso + "\n\n");
        }

        System.out.println("Attaque de Gimli sur Aragorn : " + ((float)equipe.get(0).getPrecision() / 100) * (100 - (float)equipe.get(1).getEsquive()) + "%");
        equipe.get(0).attaquer(equipe.get(1));
        System.out.println(equipe.get(1).getPointsDeVie() + "/" + equipe.get(1).getPointsDeVieMax());

        System.out.println("Vie de Gimli passée à 90");
        equipe.get(0).setPointsDeVie(90);
        System.out.println(equipe.get(0).getPointsDeVie() + "/" + equipe.get(0).getPointsDeVieMax());

        System.out.println("Soin de Galadriel sur Gimli");
        equipe.get(2).soigner(equipe.get(0));
        System.out.println(equipe.get(0).getPointsDeVie() + "/" + equipe.get(0).getPointsDeVieMax());

        System.out.println("Defense de Bilbon sur Gandalf");
        equipe.get(3).defendre(equipe.get(4));
        System.out.println(equipe.get(3).getDefense() + "\n\n");
        System.out.println(equipe.get(4).getDefense() + "\n\n");
        System.out.println("Permutation de défense");
        equipe.get(4).defendre(equipe.get(3));
        System.out.println(equipe.get(3).getDefense() + "\n\n");
        System.out.println(equipe.get(4).getDefense() + "\n\n");
        System.out.println("Repli de Gandalf");
        equipe.get(4).seReplier();
        System.out.println(equipe.get(3).getDefense() + "\n\n");
        System.out.println(equipe.get(4).getDefense() + "\n\n");

        System.out.println("Fouille basique");
        equipe.get(2).fouiller();
        System.out.println((equipe.get(2).getInventaire().size() > 0) ? equipe.get(2).getInventaire().get(0).getClass() : "Rien");

        System.out.println("Fouille voleur");
        equipe.get(3).fouiller();
        System.out.println((equipe.get(3).getInventaire().size() > 0) ? equipe.get(3).getInventaire().get(0).getClass() : "Rien");


        System.out.println("Vie de Gimli passée à 75");
        equipe.get(0).setPointsDeVie(75);
        System.out.println(equipe.get(0).getPointsDeVie() + "/" + equipe.get(0).getPointsDeVieMax());

        System.out.println("Test potion de soin");
        PotionSoin potionSoin = new PotionSoin();
        System.out.println("On essaie de l'utiliser alors qu'on ne l'a pas encore");
        equipe.get(0).utiliserObjet(potionSoin);
        System.out.println(equipe.get(0).getPointsDeVie() + "/" + equipe.get(0).getPointsDeVieMax());
        System.out.println("On l'utilise");
        equipe.get(0).getInventaire().add(potionSoin);
        equipe.get(0).utiliserObjet(potionSoin);
        System.out.println(equipe.get(0).getPointsDeVie() + "/" + equipe.get(0).getPointsDeVieMax());

        System.out.println("Précision de Gimli : " + equipe.get(0).getPrecision());
        System.out.println("Test potion de précision");
        PotionPrecision potionPrecision = new PotionPrecision();
        equipe.get(0).getInventaire().add(potionPrecision);
        equipe.get(0).utiliserObjet(potionPrecision);
        System.out.println("Précision de Gimli : " + equipe.get(0).getPrecision());

        System.out.println("Esquive de Gimli : " + equipe.get(0).getEsquive());
        System.out.println("Test potion d'esquive");
        PotionEsquive potionEsquive = new PotionEsquive();
        equipe.get(0).getInventaire().add(potionEsquive);
        equipe.get(0).utiliserObjet(potionEsquive);
        System.out.println("Esquive de Gimli : " + equipe.get(0).getEsquive());

        System.out.println("Test du bouclier \nAragorn gagne un bouclier");
        equipe.get(1).getInventaire().add(new Bouclier());
        System.out.println((equipe.get(1).getInventaire().size() > 0) ? equipe.get(1).getInventaire().get(0).getClass() : "Rien");
        System.out.println("Attaque de Gimli sur Aragorn : " + ((float)equipe.get(0).getPrecision() / 100) * (100 - (float)equipe.get(1).getEsquive()) + "%");
        equipe.get(0).attaquer(equipe.get(1));
        System.out.println(equipe.get(1).getPointsDeVie() + "/" + equipe.get(1).getPointsDeVieMax());
        System.out.println("Aragorn a perdu son bouclier :");
        System.out.println((equipe.get(1).getInventaire().size() > 0) ? equipe.get(1).getInventaire().get(0).getClass() : "Rien");

        System.out.println("Test de l'arme \nGimli gagne une arme");
        equipe.get(0).getInventaire().add(new Arme());
        System.out.println((equipe.get(0).getInventaire().size() > 0) ? equipe.get(0).getInventaire().get(0).getClass() : "Rien");
        System.out.println("Attaque de Gimli sur Aragorn : " + ((float)equipe.get(0).getPrecision() / 100) * (100 - (float)equipe.get(1).getEsquive()) + "%");
        equipe.get(0).attaquer(equipe.get(1));
        System.out.println(equipe.get(1).getPointsDeVie() + "/" + equipe.get(1).getPointsDeVieMax());
        System.out.println("Gimli a perdu son arme :");
        System.out.println((equipe.get(1).getInventaire().size() > 0) ? equipe.get(1).getInventaire().get(0).getClass() : "Rien");

    }

}
