package pokebash.ihm;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.Box;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

import java.util.Collections;
import java.util.ArrayList;

import pokebash.Pokebash;
import pokebash.personnage.Personnage;
import pokebash.jeu.JoueurPokebash;
import pokebash.personnage.action.ChoixAction;
import pokebash.personnage.action.soin.AucunSoin;
import pokebash.consommable.Consommable;
import pokebash.consommable.Arme;
import pokebash.consommable.Bouclier;
import pokebash.consommable.Potion;


public class Fenetre extends JFrame{

    private JPanel container = new JPanel();
    private JPanel panJ1 = new JPanel();
    private JPanel panJ2 = new JPanel();
    private JPanel menu = new JPanel();
    private JTextPane informations = new JTextPane();
    private BoutonMenu[] boutonsMenu = {new BoutonInventaire(),
                                        new BoutonDonnerObjet(),
                                        new BoutonAttaquer(),
                                        new BoutonDefendre(),
                                        new BoutonRepli(),
                                        new BoutonSoigner(),
                                        new BoutonFouiller()};
    private BoutonPerso boutonPersoActive = null;
    private BoutonMenu boutonMenuActive = null;
    private BoutonObjet boutonObjetActive = null;
    private ArrayList<BoutonPerso> placementsJ1 = new ArrayList<BoutonPerso>(), placementsJ2 = new ArrayList<BoutonPerso>();
    private Pokebash jeu;


    public Fenetre(Pokebash jeu){
        this.jeu = jeu;
        this.setTitle("Pokebash");
        this.setSize(900, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(true);
        this.initialiserComposants(jeu);
        this.setContentPane(container);
        this.setVisible(true);
    }

    private void initialiserComposants(Pokebash jeu){
        JoueurPokebash joueur1 = (JoueurPokebash)(jeu.getJoueurs().get(0));
        JoueurPokebash joueur2 = (JoueurPokebash)(jeu.getJoueurs().get(1));

        // placement des personnages
        for (int i = 0 ; i <= 2 ; i++){
            placementsJ1.add(new BoutonPerso(joueur1.getEquipe().get(i)));
            placementsJ1.add(new BoutonPerso());
        }

        for (int i = 0 ; i <= 2 ; i++){
            placementsJ2.add(new BoutonPerso());
            placementsJ2.add(new BoutonPerso(joueur2.getEquipe().get(i)));
        }

        // construction dans panneau J1
        panJ1.setPreferredSize(new Dimension(300, 500));
        panJ1.setLayout(new GridLayout(3, 2));
        for (int i = 0 ; i <= 5 ; i++){
            panJ1.add(placementsJ1.get(i));
        }

        // construction dans panneau J1
        panJ2.setPreferredSize(new Dimension(300, 500));
        panJ2.setLayout(new GridLayout(3, 2));
        for (int i = 0 ; i <= 5 ; i++){
            panJ2.add(placementsJ2.get(i));
        }

        // construction du menu
        menu.setPreferredSize(new Dimension(300, 500));
        menu.setLayout(new GridLayout(2,1));
        menu.add(informations);
        menu.add(this.menuPerso());

        Box agencement = Box.createHorizontalBox();
        agencement.add(panJ1);
        agencement.add(menu);
        agencement.add(panJ2);

        container.setLayout(new BorderLayout());
        container.add(agencement, BorderLayout.CENTER);
    }

    public void ecranVictoire(){
        container.removeAll();
        container.setLayout(new BorderLayout());
        container.add(new JLabel(jeu.joueurSuivant().getNom() + " a gagné !!!", JLabel.CENTER), BorderLayout.CENTER);
        container.revalidate();
        container.repaint();
    }

    private ArrayList<BoutonPerso> boutonsJoueurCourant(Pokebash jeu){
        if (jeu.getJoueurCourant().equals(jeu.getJoueurs().get(0))){
            return this.placementsJ1;
        }
        else {
            return this.placementsJ2;
        }
    }

    private ArrayList<BoutonPerso> boutonsAutreJoueur(Pokebash jeu){
        if (jeu.getJoueurCourant().equals(jeu.getJoueurs().get(0))){
            return this.placementsJ2;
        }
        else {
            return this.placementsJ1;
        }
    }

    public void majFenetreNouveauPerso(Pokebash jeu){
        // recherche des bouton enabled
        ArrayList<BoutonPerso> boutonsJoueurCourant, boutonsAutreJoueur;

        boutonsJoueurCourant = this.boutonsJoueurCourant(jeu);
        boutonsAutreJoueur = this.boutonsAutreJoueur(jeu);

        for (BoutonPerso bouton : boutonsJoueurCourant)
            bouton.desactiver();

        for (BoutonPerso bouton : boutonsJoueurCourant){
            for (String nomPerso : ((JoueurPokebash) jeu.getJoueurCourant()).getPersosAJouer()){
                if ((bouton.getPersonnage() != null) && (bouton.getPersonnage().getNom() == nomPerso))
                    bouton.activer();
            }
        }

        for (BoutonPerso bouton : boutonsAutreJoueur){
            bouton.desactiver();
        }

        for (BoutonMenu bouton : boutonsMenu){
            bouton.setImage(bouton.getImageDefaut());
            bouton.setEnabled(false);
        }

        panJ1.removeAll();
        for (int i = 0 ; i <= 5 ; i++){
            panJ1.add(placementsJ1.get(i));
        }
        panJ1.revalidate();
        panJ1.repaint();

        panJ2.removeAll();
        for (int i = 0 ; i <= 5 ; i++){
            panJ2.add(placementsJ2.get(i));
        }
        panJ2.revalidate();
        panJ2.repaint();

        informations.setEditable(false);
    }

    private JPanel menuPerso(){
        JPanel panActions = new JPanel();
        panActions.setLayout(new GridLayout(7,1));
        for (BoutonMenu bouton : boutonsMenu){
            panActions.add(bouton);
        }
        return panActions;
    }

    private JPanel menuInventaire(Personnage personnage){
        JPanel panObjets = new JPanel();
        panObjets.setLayout(new GridLayout(personnage.getInventaire().size() + 1, 1));
        for (Consommable consommable : personnage.getInventaire())
            panObjets.add(new BoutonObjet(consommable));
        panObjets.add(new BoutonRetour());
        return panObjets;
    }




    public class BoutonPerso extends JButton implements MouseListener{
        private Personnage personnage;
        private Image image, imageDefaut, imageSelectionnee;


        public BoutonPerso(){
            super();
            this.personnage = null;
            this.setEnabled(false);
        }

        public BoutonPerso(Personnage personnage){
            super(personnage.getNom());
            this.personnage = personnage;
            this.setEnabled(true);
            this.addMouseListener(this);
            try {
                imageDefaut = ImageIO.read(new File("ressources/fondVert.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                imageSelectionnee = ImageIO.read(new File("ressources/fondOrange.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            image = imageDefaut;
        }

        public void activer(){
            this.setEnabled(true);
            image = imageDefaut;
        }

        public void desactiver(){
            this.setEnabled(false);
            image = null;
        }

        public Personnage getPersonnage(){
            return this.personnage;
        }

        public void setPersonnage(Personnage personnage){
            this.personnage = personnage;
        }

        public Image getImage(){
            return this.image;
        }

        public void setImage(Image image){
            this.image = image;
        }

        public Image getImageDefaut(){
            return this.imageDefaut;
        }

        public void setImageDefaut(Image image){
            this.imageDefaut = image;
        }

        public Image getImageSelectionnee(){
            return this.imageSelectionnee;
        }

        public void setImageSelectionnee(Image image){
            this.imageSelectionnee = image;
        }


        public void paintComponent(Graphics g){
            Graphics2D g2d = (Graphics2D)g;
            GradientPaint gp = new GradientPaint(0, 0, Color.blue, 0, 20, Color.cyan, true);
            g2d.setPaint(gp);
            if (this.image != null)
                g2d.drawImage(this.image, 0, 0, this.getWidth(), this.getHeight(), this);
            g2d.setColor(Color.black);
            if (this.personnage != null)
                g2d.drawString(this.personnage.getNom(), this.getWidth() / 2 - (this.getWidth() / 2 /4), (this.getHeight() / 2) + 5);
        }


        public void mouseClicked(MouseEvent event) {
            if (this.isEnabled()){
                if (boutonMenuActive == null) {
                    if (boutonPersoActive != null){
                        boutonPersoActive.setImage(boutonPersoActive.getImageDefaut());
                        boutonPersoActive.repaint();
                    }
                    image = imageSelectionnee;
                    boutonPersoActive = this;



                    for (BoutonMenu bouton : boutonsMenu){
                        if ((bouton.getActionBouton() == ChoixAction.SOIGNER) && (personnage.getSoin() instanceof AucunSoin)){
                            bouton.setEnabled(false);
                        }
                        else if ((bouton.getActionBouton() == ChoixAction.SE_REPLIER) && (personnage.getDefense().getDefendu() == null)){
                            bouton.setEnabled(false);
                        }
                        else if ((bouton.getActionBouton() == ChoixAction.DEFENDRE) && ((personnage.getDefense().getDefenseur() != null) || (personnage.getDefense().getDefendu() != null))){
                            bouton.setEnabled(false);
                        }
                        else {
                            bouton.setEnabled(true);
                        }
                        bouton.repaint();
                    }
                    container.repaint();
                }
                else {
                    boutonMenuActive.resolutionAction(this.getPersonnage());
                }
            }
        }

        public void mouseEntered(MouseEvent event) {
            if (boutonMenuActive == null)
                informations.setText(this.personnage.resumePersonnage());
            else {
                informations.setText(boutonMenuActive.resumeEffet(this.personnage));
            }
        }

        public void mouseExited(MouseEvent event) {
            informations.setText("");
        }

        public void mousePressed(MouseEvent event) {
        }

        public void mouseReleased(MouseEvent event) {

        }
    }







    public abstract class BoutonMenu extends JButton implements MouseListener{
        private Image image, imageDefaut, imageSelectionnee;
        private ChoixAction action;

        public BoutonMenu(ChoixAction action){
            super(action.toString());
            this.setEnabled(false);
            this.action = action;

            try {
                imageDefaut = ImageIO.read(new File("ressources/fondJaune.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                imageSelectionnee = ImageIO.read(new File("ressources/fondOrange.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            image = imageDefaut;

            this.addMouseListener(this);
        }

        public ChoixAction getActionBouton(){
            return action;
        }

        public Image getImage(){
            return this.image;
        }

        public void setImage(Image image){
            this.image = image;
        }

        public Image getImageDefaut(){
            return this.imageDefaut;
        }

        public void setImageDefaut(Image image){
            this.imageDefaut = image;
        }

        public Image getImageSelectionnee(){
            return this.imageSelectionnee;
        }

        public void setImageSelectionnee(Image image){
            this.imageSelectionnee = image;
        }

        public abstract void preparationFenetreAction();

        public abstract void resolutionAction(Personnage personnageVise); // peut etre null

        public abstract String resumeEffet(Personnage personnageVise);

        public void remiseAJour(){
            if (((JoueurPokebash) jeu.joueurSuivant()).aPerdu()){
                ((JoueurPokebash) jeu.getJoueurCourant()).getPersosAJouer().clear();
            }
            else{
                ((JoueurPokebash) jeu.getJoueurCourant()).getPersosAJouer().remove(boutonPersoActive.getPersonnage().getNom());
                boutonPersoActive = null;
                boutonMenuActive = null;
                majFenetreNouveauPerso(jeu);
                container.repaint();
            }
        }

        public void paintComponent(Graphics g){
            Graphics2D g2d = (Graphics2D)g;
            if (this.isEnabled()){
                GradientPaint gp = new GradientPaint(0, 0, Color.blue, 0, 20, Color.cyan, true);
                g2d.setPaint(gp);
                g2d.drawImage(this.image, 0, 0, this.getWidth(), this.getHeight(), this);
                g2d.setColor(Color.black);
            }
            if (this.action != null)
                g2d.drawString(action.toString(), this.getWidth() / 2 - (this.getWidth() / 2 /4), (this.getHeight() / 2) + 5);
        }


        public void mouseClicked(MouseEvent event) {
            if (this.isEnabled()){
                if (boutonMenuActive != null){
                    boutonMenuActive.setImage(boutonMenuActive.getImageDefaut());
                    boutonMenuActive.repaint();
                }
                image = imageSelectionnee;
                boutonMenuActive = this;

                for (BoutonPerso bouton : placementsJ1){
                    if (bouton != boutonPersoActive)
                        bouton.desactiver();
                    else
                        bouton.setEnabled(false);
                    bouton.repaint();
                }

                for (BoutonPerso bouton : placementsJ2){
                    if (bouton != boutonPersoActive)
                        bouton.desactiver();
                    else
                        bouton.setEnabled(false);
                    bouton.repaint();
                }
                container.repaint();

                this.preparationFenetreAction();
            }
        }

        public void mouseEntered(MouseEvent event) {

        }

        public void mouseExited(MouseEvent event) {

        }

        public void mousePressed(MouseEvent event) {
        }

        public void mouseReleased(MouseEvent event) {

        }
    }

    public class BoutonInventaire extends BoutonMenu {

        public BoutonInventaire(){
            super(ChoixAction.INVENTAIRE);
        }

        public void preparationFenetreAction(){
            this.resolutionAction(boutonPersoActive.getPersonnage());
        }

        public void resolutionAction(Personnage personnageAPourvoir){
            menu.remove(1); // les boutons d'interface
            menu.add(menuInventaire(personnageAPourvoir));
            menu.revalidate();
            menu.repaint();
        }

        public String resumeEffet(Personnage personnageVise){
            return personnageVise.getNom() + "\n";
        }
    }

    public class BoutonDonnerObjet extends BoutonMenu {

        public BoutonDonnerObjet(){
            super(ChoixAction.DONNER);
        }

        public void preparationFenetreAction(){
            menu.remove(1); // les boutons d'interface
            menu.add(menuInventaire(boutonPersoActive.getPersonnage()));
            menu.revalidate();
            menu.repaint();
        }

        public void resolutionAction(Personnage personnageAPourvoir){
            boutonPersoActive.getPersonnage().donnerObjet(boutonObjetActive.getObjet(), personnageAPourvoir);

            menu.remove(1); // les boutons d'interface
            menu.add(menuPerso());
            menu.revalidate();
            menu.repaint();

            boutonPersoActive = null;
            boutonMenuActive = null;
            boutonObjetActive = null;
            majFenetreNouveauPerso(jeu);
            container.repaint();
        }

        public String resumeEffet(Personnage personnageVise){
            return "";
        }

    }

    public class BoutonAttaquer extends BoutonMenu {

        public BoutonAttaquer(){
            super(ChoixAction.ATTAQUER);
        }

        public void preparationFenetreAction(){
            for (BoutonPerso bouton : boutonsAutreJoueur(jeu)){
                if ((bouton.getPersonnage() != null) && (bouton.getPersonnage().getDefense().getDefenseur() == null))
                    bouton.activer();
            }
        }

        public void resolutionAction(Personnage personnageVise){
            Personnage persoActif = boutonPersoActive.getPersonnage();
            persoActif.attaquer(personnageVise);
            if (personnageVise.getPointsDeVie() == 0){
                BoutonPerso boutonAEnlever = null;
                ArrayList<BoutonPerso> boutonsAutreJoueur = boutonsAutreJoueur(jeu);
                for (BoutonPerso bouton : boutonsAutreJoueur){
                    if (bouton.getPersonnage() == personnageVise)
                        boutonAEnlever = bouton;
                }
                personnageVise.seReplier(); //au cas où il était devant un autre perso
                int indice = boutonsAutreJoueur.indexOf(boutonAEnlever);
                boutonsAutreJoueur.remove(boutonAEnlever);
                boutonsAutreJoueur.add(indice, new BoutonPerso());
            }

            this.remiseAJour();
        }

        public String resumeEffet(Personnage personnageVise){
            if (((JoueurPokebash) jeu.getJoueurCourant()).getEquipe().contains(personnageVise)){
                return personnageVise.resumePersonnage();
            }
            else {
                Personnage attaquant = boutonPersoActive.getPersonnage();
                int degatsAttaque = attaquant.getAttaque().attaquer();
                if (attaquant.getInventaire().contains(new Arme())) {
                    degatsAttaque += Arme.DEGATS;
                }
                int vieRestante = Math.max(0, personnageVise.getPointsDeVie() - degatsAttaque);
                int pourcentage = (int) (((float) attaquant.getPrecision()) * ((100 - (float) personnageVise.getEsquive()) / 100));
                return attaquant.getNom() + " attaque " + personnageVise.getNom() + "\n\nPoints de vie : " +
                        personnageVise.getPointsDeVie() + "/" + personnageVise.getPointsDeVieMax() +
                        " -> " + vieRestante + "/" + personnageVise.getPointsDeVieMax() +
                        "\n\nPourcentage de réussite : " + pourcentage + "%";
            }
        }
    }

    public class BoutonDefendre extends BoutonMenu {

        public BoutonDefendre(){
            super(ChoixAction.DEFENDRE);
        }

        public void preparationFenetreAction(){
            Personnage persoActif = boutonPersoActive.getPersonnage(), persoASelectionner;
            for (BoutonPerso bouton : boutonsJoueurCourant(jeu)){
                persoASelectionner = bouton.getPersonnage();
                if (persoASelectionner != null){
                    if ((persoASelectionner != persoActif) &&
                        (persoASelectionner.getDefense().getDefendu() == null) &&
                        (persoASelectionner.getDefense().getDefenseur() == null)){
                        bouton.activer();
                    }
                }
            }

        }

        public void resolutionAction(Personnage personnageVise){
            // les boutons visés ne peuvent être que en deuxieme ligne
            int nouvellePlace = 0;
            ArrayList<BoutonPerso> boutonsJoueurCourant = boutonsJoueurCourant(jeu);
            for (BoutonPerso bouton : boutonsJoueurCourant){
                if (bouton.getPersonnage() == personnageVise)
                    nouvellePlace = boutonsJoueurCourant.indexOf(bouton) + ((boutonsJoueurCourant == placementsJ1) ? 1 : -1);
            }
            Collections.swap(boutonsJoueurCourant, boutonsJoueurCourant.indexOf(boutonPersoActive), nouvellePlace);

            boutonPersoActive.getPersonnage().defendre(personnageVise);

            //((JoueurPokebash) jeu.getJoueurCourant()).getPersosAJouer().remove(personnageVise.getNom());

            this.remiseAJour();
        }

        public String resumeEffet(Personnage personnageVise){
            if ((((JoueurPokebash) jeu.getJoueurCourant()).getEquipe().contains(personnageVise)) && (personnageVise != boutonPersoActive.getPersonnage())){
                return boutonPersoActive.getPersonnage().getNom() + " part défendre " + personnageVise.getNom();

            }
            else {
                return personnageVise.resumePersonnage();
            }
        }

    }

    public class BoutonRepli extends BoutonMenu {

        public BoutonRepli(){
            super(ChoixAction.SE_REPLIER);
        }

        public void preparationFenetreAction(){
            resolutionAction(boutonPersoActive.getPersonnage());
        }


        public void resolutionAction(Personnage personnageVise){
            int nouvellePosition = 0, placeTemp;
            ArrayList<BoutonPerso> boutonsJoueurCourant = boutonsJoueurCourant(jeu);
            for (int i = 0 ; i <= 2 ; i++){
                placeTemp = 2 * i + ((boutonsJoueurCourant == placementsJ1) ? 0 : 1);
                System.out.println(placeTemp + " " + (boutonsJoueurCourant.get(placeTemp).getPersonnage() == null));
                if (boutonsJoueurCourant.get(placeTemp).getPersonnage() == null){
                    nouvellePosition = placeTemp;
                }
            }
            Collections.swap(boutonsJoueurCourant, boutonsJoueurCourant.indexOf(boutonPersoActive), nouvellePosition);

            boutonPersoActive.getPersonnage().seReplier();

            this.remiseAJour();
        }

        public String resumeEffet(Personnage personnageVise){
            return "";
        }

    }

    public class BoutonSoigner extends BoutonMenu {

        public BoutonSoigner(){
            super(ChoixAction.SOIGNER);
        }

        public void preparationFenetreAction(){
            Personnage persoActif = boutonPersoActive.getPersonnage(), persoASelectionner;
            for (BoutonPerso bouton : boutonsJoueurCourant(jeu)){
                persoASelectionner = bouton.getPersonnage();
                if ((persoASelectionner != null) && (persoASelectionner != persoActif)){
                    bouton.activer();
                }
            }
        }

        public void resolutionAction(Personnage personnageVise){
            Personnage persoActif = boutonPersoActive.getPersonnage();
            persoActif.soigner(personnageVise);

            this.remiseAJour();
        }

        public String resumeEffet(Personnage personnageVise){
            if ((((JoueurPokebash) jeu.getJoueurCourant()).getEquipe().contains(personnageVise)) && (personnageVise != boutonPersoActive.getPersonnage())){
                int vieAtteinte = Math.min(personnageVise.getPointsDeVieMax(), personnageVise.getPointsDeVie() + boutonPersoActive.getPersonnage().getSoin().soigner());
                return personnageVise.getNom() + "\n\n Points de vie : " +
                        personnageVise.getPointsDeVie() + "/" + personnageVise.getPointsDeVieMax() +
                        " -> " + vieAtteinte + "/" + personnageVise.getPointsDeVieMax();

            }
            else {
                return personnageVise.resumePersonnage();
            }
        }

    }

    public class BoutonFouiller extends BoutonMenu {

        public BoutonFouiller(){
            super(ChoixAction.FOUILLER);
        }

        public void preparationFenetreAction(){
            resolutionAction(boutonPersoActive.getPersonnage());
        }

        public void resolutionAction(Personnage personnageVise){
            personnageVise.fouiller();
            this.remiseAJour();
        }

        public String resumeEffet(Personnage personnageVise){
            return "";
        }

    }

    public abstract class BoutonSacoche extends JButton implements MouseListener {

        public BoutonSacoche(String nom){
            super(nom);
            this.setEnabled(false);
            this.addMouseListener(this);
        }

        public abstract void resolution(Personnage personnageVise); // peut etre null

        public void mouseClicked(MouseEvent event) {
            if (this.isEnabled()){
                this.resolution(boutonPersoActive.getPersonnage());
            }
        }

        public void mouseEntered(MouseEvent event) {

        }

        public void mouseExited(MouseEvent event) {

        }

        public void mousePressed(MouseEvent event) {
        }

        public void mouseReleased(MouseEvent event) {

        }
    }

    public class BoutonRetour extends BoutonSacoche{
        public BoutonRetour(){
            super("Retour");
            this.setEnabled(true);
        }


        public void resolution(Personnage personnageVise){
            menu.remove(1); // les boutons d'interface
            menu.add(menuPerso());
            menu.revalidate();
            menu.repaint();

            boutonPersoActive = null;
            boutonMenuActive = null;
            majFenetreNouveauPerso(jeu);
            container.repaint();
            // remiseAJour mais sans enlever le perso
        }
    }

    public class BoutonObjet extends BoutonSacoche{

        private Consommable objet;

        public BoutonObjet(Consommable objet){
            super(objet.getNom());
            this.objet = objet;
            if (boutonMenuActive instanceof BoutonInventaire)
                this.setEnabled(! ((objet instanceof Arme) || (objet instanceof Bouclier)));
            else
                this.setEnabled(true);
        }

        public Consommable getObjet(){
            return this.objet;
        }

        public void resolution(Personnage personnageVise){
            if (boutonMenuActive instanceof BoutonInventaire){
                menu.remove(1); // les boutons d'interface
                menu.add(menuPerso());
                menu.revalidate();
                menu.repaint();

                boutonPersoActive.getPersonnage().utiliserObjet((Potion) this.objet);
                boutonPersoActive = null;
                boutonMenuActive = null;
                majFenetreNouveauPerso(jeu);
                container.repaint();
                // remiseAJour mais sans enlever le perso
            }
            else {
                Personnage persoActif = boutonPersoActive.getPersonnage(), persoASelectionner;
                for (BoutonPerso bouton : boutonsJoueurCourant(jeu)){
                    persoASelectionner = bouton.getPersonnage();
                    if ((persoASelectionner != null) && (persoASelectionner != persoActif)){
                        bouton.activer();
                    }
                }
                boutonObjetActive = this;
            }
        }
    }


}
