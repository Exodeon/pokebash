# pokebash

Mini-projet de Java GM3. Jeu de combat au tour par tour avec une équipe de personnages

Commande pour compiler :
```bash
javac -classpath ./classes -sourcepath ./src -d ./classes <fichier à compiler>
```

Commande pour exécuter :
```bash
java -classpath ./classes <fichier à exécuter>
```

Diagramme de classe : https://online.visual-paradigm.com/w/nomgqxyg/diagrams.jsp#diagram:proj=0&id=1
